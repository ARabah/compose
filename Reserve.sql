--
-- PostgreSQL database dump
--

-- Dumped from database version 15.6 (Debian 15.6-1.pgdg120+2)
-- Dumped by pg_dump version 15.6 (Debian 15.6-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cinema; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.cinema (
    id integer NOT NULL
);


ALTER TABLE public.cinema OWNER TO ynov;

--
-- Name: cinema_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.cinema_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cinema_id_seq OWNER TO ynov;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO ynov;

--
-- Name: reservation; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.reservation (
    id integer NOT NULL,
    uid uuid NOT NULL,
    rank smallint NOT NULL,
    status character varying(255) NOT NULL,
    seats smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    expires_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.reservation OWNER TO ynov;

--
-- Name: COLUMN reservation.uid; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.reservation.uid IS '(DC2Type:uuid)';


--
-- Name: reservation_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.reservation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_id_seq OWNER TO ynov;

--
-- Name: room; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.room (
    id integer NOT NULL,
    uid uuid NOT NULL,
    name character varying(255) NOT NULL,
    seats smallint NOT NULL
);


ALTER TABLE public.room OWNER TO ynov;

--
-- Name: COLUMN room.uid; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.room.uid IS '(DC2Type:uuid)';


--
-- Name: room_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.room_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.room_id_seq OWNER TO ynov;

--
-- Name: sceance; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.sceance (
    id integer NOT NULL,
    uid uuid NOT NULL,
    movies uuid NOT NULL,
    ddate date NOT NULL
);


ALTER TABLE public.sceance OWNER TO ynov;

--
-- Name: COLUMN sceance.uid; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.sceance.uid IS '(DC2Type:uuid)';


--
-- Name: COLUMN sceance.movies; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.sceance.movies IS '(DC2Type:uuid)';


--
-- Name: sceance_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.sceance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sceance_id_seq OWNER TO ynov;

--
-- Data for Name: cinema; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.cinema (id) FROM stdin;
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20240305080718	2024-03-05 08:11:48	40
\.


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.reservation (id, uid, rank, status, seats, created_at, updated_at, expires_at) FROM stdin;
\.


--
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.room (id, uid, name, seats) FROM stdin;
\.


--
-- Data for Name: sceance; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.sceance (id, uid, movies, ddate) FROM stdin;
\.


--
-- Name: cinema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.cinema_id_seq', 1, false);


--
-- Name: reservation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.reservation_id_seq', 1, false);


--
-- Name: room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.room_id_seq', 1, false);


--
-- Name: sceance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.sceance_id_seq', 1, false);


--
-- Name: cinema cinema_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.cinema
    ADD CONSTRAINT cinema_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


--
-- Name: room room_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT room_pkey PRIMARY KEY (id);


--
-- Name: sceance sceance_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.sceance
    ADD CONSTRAINT sceance_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

