--
-- PostgreSQL database dump
--

-- Dumped from database version 15.6 (Debian 15.6-1.pgdg120+2)
-- Dumped by pg_dump version 15.6 (Debian 15.6-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: brut_force_entity; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.brut_force_entity (
    id integer NOT NULL,
    ip character varying(100) NOT NULL,
    fakepassword character varying(255) NOT NULL,
    fakelogin character varying(255) NOT NULL,
    delais timestamp(0) without time zone NOT NULL,
    retry integer NOT NULL,
    block_ip boolean NOT NULL,
    etat integer NOT NULL,
    initdatetime character varying(255) NOT NULL
);


ALTER TABLE public.brut_force_entity OWNER TO ynov;

--
-- Name: brut_force_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.brut_force_entity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.brut_force_entity_id_seq OWNER TO ynov;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO ynov;

--
-- Name: token; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.token (
    id integer NOT NULL,
    utilisateur_id integer,
    access_token character varying(255) DEFAULT NULL::character varying,
    access_token_expires_at date,
    refresh_token character varying(255) NOT NULL,
    refresh_token_expires_at date NOT NULL
);


ALTER TABLE public.token OWNER TO ynov;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.token_id_seq OWNER TO ynov;

--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.utilisateur (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    login character varying(255) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    create_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.utilisateur OWNER TO ynov;

--
-- Name: COLUMN utilisateur.create_at; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.utilisateur.create_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN utilisateur.updated_at; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.utilisateur.updated_at IS '(DC2Type:datetime_immutable)';


--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utilisateur_id_seq OWNER TO ynov;

--
-- Data for Name: brut_force_entity; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.brut_force_entity (id, ip, fakepassword, fakelogin, delais, retry, block_ip, etat, initdatetime) FROM stdin;
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
\.


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.token (id, utilisateur_id, access_token, access_token_expires_at, refresh_token, refresh_token_expires_at) FROM stdin;
1	\N	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NGUyY2U3Ni1hNDE0LTQ0ZGMtYmQyZi05OGViZmFhMmIyMjIiLCJpYXQiOiIxNzAyOTk5NjA3In0.td049P9j7lypj1YB73xG7NnOXifVb66E4_TE0vjV-DI	2023-12-19	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NGUyY2U3Ni1hNDE0LTQ0ZGMtYmQyZi05OGViZmFhMmIyMjIiLCJpYXQiOiIxNzAzMDAzMjA3In0.Oh0cHlBDlYS_WbM9XRZoeydNjAEdXxzdXcxPNfcHlPI	2023-12-19
\.


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.utilisateur (id, uuid, login, roles, password, status, create_at, updated_at) FROM stdin;
\.


--
-- Name: brut_force_entity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.brut_force_entity_id_seq', 1, false);


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.token_id_seq', 1, true);


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.utilisateur_id_seq', 1, false);


--
-- Name: brut_force_entity brut_force_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.brut_force_entity
    ADD CONSTRAINT brut_force_entity_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: token token_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: utilisateur utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: uniq_5f37a13bfb88e14f; Type: INDEX; Schema: public; Owner: ynov
--

CREATE UNIQUE INDEX uniq_5f37a13bfb88e14f ON public.token USING btree (utilisateur_id);


--
-- Name: token fk_5f37a13bfb88e14f; Type: FK CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT fk_5f37a13bfb88e14f FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);


--
-- PostgreSQL database dump complete
--

