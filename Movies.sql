--
-- PostgreSQL database dump
--

-- Dumped from database version 15.6 (Debian 15.6-1.pgdg120+2)
-- Dumped by pg_dump version 15.6 (Debian 15.6-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: categorie; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.categorie (
    id integer NOT NULL,
    libelle character varying(255) NOT NULL
);


ALTER TABLE public.categorie OWNER TO ynov;

--
-- Name: categorie_film; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.categorie_film (
    categorie_id integer NOT NULL,
    film_id integer NOT NULL
);


ALTER TABLE public.categorie_film OWNER TO ynov;

--
-- Name: categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorie_id_seq OWNER TO ynov;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO ynov;

--
-- Name: film; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.film (
    id integer NOT NULL,
    nom character varying(128) DEFAULT NULL::character varying,
    description character varying(4096) DEFAULT NULL::character varying,
    date date,
    note smallint,
    uid uuid,
    created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    duration character varying(255) NOT NULL
);


ALTER TABLE public.film OWNER TO ynov;

--
-- Name: COLUMN film.uid; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.film.uid IS '(DC2Type:uuid)';


--
-- Name: COLUMN film.duration; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.film.duration IS '(DC2Type:dateinterval)';


--
-- Name: film_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.film_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.film_id_seq OWNER TO ynov;

--
-- Data for Name: categorie; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.categorie (id, libelle) FROM stdin;
1	action
2	fantésie
\.


--
-- Data for Name: categorie_film; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.categorie_film (categorie_id, film_id) FROM stdin;
1	1
2	1
1	2
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
\.


--
-- Data for Name: film; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.film (id, nom, description, date, note, uid, created_at, updated_at, duration) FROM stdin;
1	Inception	Un thriller de science-fiction sur les rêves.	2010-07-16	5	fa9f2c73-9c21-4b84-803f-c5e9d7b14a35	2024-03-05 08:37:19	2024-03-05 08:37:19	+P00Y00M01DT00H00M00S
2	The Dark Knight	Un film de super-héros basé sur le personnage de Batman	2008-07-18	5	0e9b603b-e58c-4dd3-9560-87e755cc1c5d	2024-03-05 08:42:01	2024-03-05 08:42:01	+P00Y00M01DT00H00M00S
3	Pulp Fiction	Une série d'histoires interconnectées dans le monde du crime.	1994-05-21	4	2661a67e-7449-4def-85cc-fad4a2055a98	2024-03-05 08:45:28	2024-03-05 08:45:28	+P00Y00M01DT00H00M00S
4	La La Land	Une romance musicale sur deux aspirants artistes à Los Angeles.	2016-08-31	3	8976f56e-6f29-4927-977c-0a9550ca415b	2024-03-05 08:45:28	2024-03-05 08:45:28	+P00Y00M01DT00H00M00S
5	test	testdescription	2023-12-11	5	30c2819a-7d4e-432e-8205-42383a1d797a	2024-03-05 08:45:28	2024-03-05 08:45:28	+P00Y00M01DT00H00M00S
\.


--
-- Name: categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.categorie_id_seq', 2, true);


--
-- Name: film_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.film_id_seq', 5, true);


--
-- Name: categorie_film categorie_film_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie_film
    ADD CONSTRAINT categorie_film_pkey PRIMARY KEY (categorie_id, film_id);


--
-- Name: categorie categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT categorie_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: film film_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_pkey PRIMARY KEY (id);


--
-- Name: idx_f56d3975567f5183; Type: INDEX; Schema: public; Owner: ynov
--

CREATE INDEX idx_f56d3975567f5183 ON public.categorie_film USING btree (film_id);


--
-- Name: idx_f56d3975bcf5e72d; Type: INDEX; Schema: public; Owner: ynov
--

CREATE INDEX idx_f56d3975bcf5e72d ON public.categorie_film USING btree (categorie_id);


--
-- Name: categorie_film fk_f56d3975567f5183; Type: FK CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie_film
    ADD CONSTRAINT fk_f56d3975567f5183 FOREIGN KEY (film_id) REFERENCES public.film(id) ON DELETE CASCADE;


--
-- Name: categorie_film fk_f56d3975bcf5e72d; Type: FK CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie_film
    ADD CONSTRAINT fk_f56d3975bcf5e72d FOREIGN KEY (categorie_id) REFERENCES public.categorie(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

